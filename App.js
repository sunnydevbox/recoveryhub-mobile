/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import Reactotron from 'reactotron-react-native';

if(__DEV__) {
  import('./src/ReactotronConfig')
    
    .then(() => console.log('Reactotron Configured'))
  const oldConsoleLog = console.log;

    /* eslint-disable */
    console.log = (...args) => {
        oldConsoleLog(...args);

        Reactotron.display({
            name: 'CONSOLE.LOG',
            value: args,
            preview: args.length > 0 && typeof args[0] === 'string' ? args[0] : null,
        });
    };
}

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';

import { reducers } from './src/Redux/index';
import { createStore, compose } from 'redux';
import { Provider } from 'react-redux';
import RootContainer from './src/Containers/RootContainer';
import SplashScreen from 'react-native-splash-screen';
// import { unregister } from './src/interceptor';

const store = createStore(reducers);

export default class App extends Component {
  componentDidMount() {
    SplashScreen.hide();
  }
  render() {
    return (
      <Provider store={store}>
        <RootContainer />
      </Provider>
    );
  }
}