let Config ={ 
    ENVIRONMENT: 'local',
    API_URL: 'http://api.recoveryhub.world/api/',
    API_HEADER_ACCEPT: 'application/x.metropsych.v1+json'
}

export default Config;