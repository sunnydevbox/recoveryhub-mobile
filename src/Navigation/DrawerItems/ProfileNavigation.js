import { createStackNavigator } from 'react-navigation'
import React, { Text, Button, Image } from 'react';
import { StyleSheet } from 'react-native';

import ProfileScreen from './../../Containers/ProfileScreen';
import DrawerButton from './../../Components/DrawerButton';


// Manifest of possible screens
export default createStackNavigator({
  ProfileScreen: {
    screen: ProfileScreen,
    name: 'Profile',
    navigationOptions: ({navigation}) => ({
      headerLeft: <DrawerButton {...{navigation: navigation}}></DrawerButton>,
      title: 'Profilee',
      drawerIcon: ({ tintColor }) => (<Text>ASd</Text>),
      //return { headerLeft, title, drawerLabel, drawerIcon };
    })
  },
})
