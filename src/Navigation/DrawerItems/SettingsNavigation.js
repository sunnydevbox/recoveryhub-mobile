import { createStackNavigator } from 'react-navigation'
import React, { Text, Button } from 'react';

import SettingsScreen from '../../Containers/SettingsScreen';
import DrawerButton from '../../Components/DrawerButton';

export default createStackNavigator({
  SettingsScreen: {
    screen: SettingsScreen,
    name: 'Settings',
    navigationOptions: ({navigation}) => ({
      headerLeft: <DrawerButton {...{navigation: navigation}}></DrawerButton>,
      title: 'Settings'
    })
  },
})
