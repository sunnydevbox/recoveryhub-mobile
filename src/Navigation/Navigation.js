import React, { Component } from 'react';
import { connect } from 'react-redux';
import InitNavigation from './InitNavigation';

class Navigation extends Component {
    render() {
        return (
            <InitNavigation />
        );
    }
}

export default Navigation;