import { createAppContainer, createStackNavigator, createSwitchNavigator } from 'react-navigation';
import SplashScreenContainer from '../Containers/SplashScreenContainer';
import DrawerNavigation from './DrawerNavigation';
import NotLoggedInNavigation from './NotLoggedInNavigation';

const PrimaryNavigation = createStackNavigator({
    LaunchScreen: { screen: SplashScreenContainer },
    LoggedInStack: { screen: DrawerNavigation },
    NotLoggedInStack: { screen: NotLoggedInNavigation },
    // Drawer: { screen: DrawerNavigator }
  }, {
    // Default config for all screens
    headerMode: 'screen',
    initialRouteName: 'LaunchScreen',
    // navigationOptions: {
    //   headerStyle: styles.header
    // }
  })


// sexport default createAppContainer(PrimaryNavigation)

export default createAppContainer(createSwitchNavigator({
    LaunchScreen: { screen: SplashScreenContainer },
    LoggedInStack: { screen: DrawerNavigation },
    NotLoggedInStack: { screen: NotLoggedInNavigation },
    // Drawer: { screen: DrawerNavigator }
  }));