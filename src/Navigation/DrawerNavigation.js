import { createDrawerNavigator, createAppContainer } from 'react-navigation';
import SideMenu from './../Components/SideMenu';
import ProfileNavigation from './DrawerItems/ProfileNavigation';
import SettingsNavigation from './DrawerItems/SettingsNavigation';

import React, { Image } from 'react';
import DrawerButton from './../Components/DrawerButton';
import { StyleSheet } from 'react-native';

export default createDrawerNavigator({
    Profile: { 
      screen: ProfileNavigation,
      navigationOptions: {
        drawerLabel: 'Profile',
      }
    }, 

    Settings: {
      screen: SettingsNavigation,
    }
}, {
    contentComponent: SideMenu,
    drawerWidth: 300,
    headerMode: 'screen',

    // Default config for all screens
    drawerPosition: 'left',

    // items: DrawerNavigationComponent,
    contentOptions: {
      activeTintColor: '#e91e63',
      itemsContainerStyle: {
        marginVertical: 0,
      },
      iconContainerStyle: {
        opacity: 1
      }
    },
    useNativeAnimations: true,
    drawerBackgroundColor: '#eee',
    drawerType: 'front',
  }

);


const styles = StyleSheet.create({
  icon: {
    width: 24,
    height: 24,
  },
});