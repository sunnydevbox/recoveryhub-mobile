import { createStackNavigator } from 'react-navigation';
import LoginScreen from './../Containers/LoginScreen';
import ProfileScreen from './../Containers/ProfileScreen';
import ForgotPasswordScreen from './../Containers/ForgotPasswordScreen';
import SignUpScreen from './../Containers/SignUpScreen';

export default createStackNavigator({
        Login: { 
            screen: LoginScreen,
        },
        ForgotPassword: {
            screen: ForgotPasswordScreen
        },
        SignUp: {
            screen: SignUpScreen
        },
    },
    {
        // Default config for all screens
        headerMode: 'none',
    }
);