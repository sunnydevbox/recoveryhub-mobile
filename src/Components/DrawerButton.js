import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Text, TouchableOpacity } from 'react-native'
import styles from './Styles/DrawerButtonStyles'
import { Icon, Button } from 'native-base';
import { DrawerActions } from 'react-navigation';

// Note that this file (App/Components/DrawerButton) needs to be
// imported in your app somewhere, otherwise your component won't be
// compiled and added to the examples dev screen.

class DrawerButton extends Component {
  static propTypes = {
    text: PropTypes.string,
    onPress: PropTypes.func
  }

  constructor(props) {
    super(props);
  }

  toggleDrawer() {

    this.props.navigation.navigat(DrawerActions.toggleDrawer());
    // this.props.navigation.navigate('DrawerToggle')
  }

  render () {
    return (
      <Button transparent onPress={() => this.props.navigation.toggleDrawer()}>
        <Icon name='menu' />
      </Button>
      
    )
  }
}

export default DrawerButton
