import PropTypes from 'prop-types';
import React, {Component} from 'react';
// import styles from './SideMenu.style';
import { NavigationActions } from 'react-navigation';
import { ScrollView, Text, View, StyleSheet, TouchableOpacity, Alert, SafeAreaView } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

import { DrawerItems } from 'react-navigation';

class SideMenu extends Component {
  navigateToScreen = (route) => () => {
    const navigateAction = NavigationActions.navigate({
      routeName: route
    });
    this.props.navigation.dispatch(navigateAction);
  }

  render () {
    let props = this.props;

    return (
      <View style={styles.container}>
        <SafeAreaView>
          <ScrollView>
            <DrawerItems {...props} />
            <TouchableOpacity onPress={()=>
              Alert.alert(
                'Log out',
                'Do you want to logout?',
                [
                  {text: 'Cancel', onPress: () => {return null}},
                  {text: 'Confirm', onPress: () => {
                    AsyncStorage.clear();
                    props.navigation.navigate('NotLoggedInStack')
                  }},
                ],
                { cancelable: false }
              )  
            }>
              <Text style={{margin: 16,fontWeight: 'bold',}}>Logout</Text>
            </TouchableOpacity>
        </ScrollView>
        </SafeAreaView>
        {/* <View style={styles.footerContainer}>
          <Text>This is my fixed footer</Text>
        </View> */}
      </View>
    );
  }
}

let styles = StyleSheet.create({
    container: {},
    sectionHeadingStyle: {},
    navSectionStyle: {},
    navItemStyle: {},
    footerContainer: {},
});

SideMenu.propTypes = {
  navigation: PropTypes.object
};

export default SideMenu;