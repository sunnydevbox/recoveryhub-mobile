import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Platform, View, StyleSheet, TouchableOpacity, ActivityIndicator,
    Image, TextInput, TouchableHighlight, Text, Dimensions, SafeAreaView, CheckBox, Switch } from 'react-native';
import { UserService } from './../Services/UserService';
import { getToken, setToken } from './../Redux/AuthRedux';
import { Images, ApplicationStyles } from './../Themes';
import FlashMessage, { showMessage, hideMessage } from 'react-native-flash-message';

function mapStateToProps(state) {
    return {
        auth: state.auth
    };
}

function mapDispatchToProps(dispatch) {
    return {

    };
}

class SignUpScreen extends Component {

    constructor(props) {
        super(props);
        this.mounted = false;
        this.state = {
            isLoading: false,
            mounted: false,
            first_name: null,
            middle_name: null,
            last_name: null,
            email: null,
            password: null,
            confirm_password: null,
            agree: true,
        };

        this.onSubmit = this.onSubmit.bind(this);
        this.apiService = new UserService();
    }

    componentDidMount() {
        this.mounted = true;
    }

    onClickListener(target) {
        this.props.navigation.replace();
    }

    onSubmit() {
        this.setState({isLoading: true});
        this.apiService.register({
            first_name: this.state.first_name,
            middle_name: this.state.middle_name,
            last_name: this.state.last_name,
            email: this.state.email,
            password: this.state.password,
            confirm_password: this.state.confirm_password,
            agree: this.state.agree,
            
        })
        .then((res) => {
            if (this.mounted) {
                this.setState({isLoading: false});

                this.props.navigation.replace('Login',{
                    registration: true,
                });
            }
            return null;
        })
        .catch((error) => {
            if (this.mounted) {
                let message = [];
                console.log(error)
                if (error.problem == 'SERVER_ERROR') {
                    message.push('We are encountering issues with our system. We could not process your request. Please try again later.');
                } else if (error.data && error.data.message) {                    
                    for (const key in error.data.message) {
                        for (let i=0; i<=error.data.message[key].length; i++) {   
                            message.push(error.data.message[key][i]);
                        }   
                    }
                }

                showMessage({
                    animationDuration: 100,
                    message: message.join("\n"),
                    type: 'danger',
                    icon: { icon: "danger", position: "left" }
                });
                
                
                this.setState({isLoading: false});
                // this.props.navigation.replace('Login',{
                //     registration: true,
                // });
            }
        });
    }

    componentWillUnmount() {
        this.mounted = false;
        hideMessage();
    }

    render() {

        const submitButton = (this.state.isLoading) ? <ActivityIndicator style={styles.loginText} /> : <Text style={styles.loginText}>Agree and Sign Up</Text>;
        let launchLogo = Images.launch;
        const dimensions = Dimensions.get('window');
        const imageHeight = Math.round(dimensions.width * 9 / 16);
        const imageWidth = dimensions.width - 100;

        const checkbox = (Platform.OS == 'ios') ? <Switch style={{height: 10}}
                                                value={this.state.agree}
                                                onValueChange={() => this.setState({ agree: !this.state.agree })}
                                                /> : <CheckBox
                                value={this.state.agree}
                                onValueChange={() => this.setState({ agree: !this.state.agree })}
                                /> 

        return (
            <SafeAreaView style={ApplicationStyles.screen.centered}>
                <Image 
                  resizeMode={'contain'}
                  source={ Images.launch } style={{width: imageWidth }} 
                />

                <View style={styles.inputContainer}>
                    {/* <Image style={styles.inputIcon} source={{uri: 'https://png.icons8.com/message/ultraviolet/50/3498db'}}/> */}
                    <TextInput style={styles.inputs}
                        placeholder="First Name"
                        keyboardType="default"
                        underlineColorAndroid='transparent'
                        onChangeText={(first_name) => this.setState({first_name})}
                    />
                </View>

                <View style={styles.inputContainer}>
                    {/* <Image style={styles.inputIcon} source={{uri: 'https://png.icons8.com/message/ultraviolet/50/3498db'}}/> */}
                    <TextInput style={styles.inputs}
                        placeholder="Middle Name"
                        keyboardType="default"
                        underlineColorAndroid='transparent'
                        onChangeText={(middle_name) => this.setState({middle_name})}
                    />
                </View>

                <View style={styles.inputContainer}>
                    {/* <Image style={styles.inputIcon} source={{uri: 'https://png.icons8.com/message/ultraviolet/50/3498db'}}/> */}
                    <TextInput style={styles.inputs}
                        placeholder="Last Name"
                        keyboardType="default"
                        underlineColorAndroid='transparent'
                        onChangeText={(last_name) => this.setState({last_name})}
                    />
                </View>
                

                <View style={styles.inputContainer}>
                    {/* <Image style={styles.inputIcon} source={{uri: 'https://png.icons8.com/message/ultraviolet/50/3498db'}}/> */}
                    <TextInput style={styles.inputs}
                        placeholder="Email"
                        keyboardType="email-address"
                        underlineColorAndroid='transparent'
                        onChangeText={(email) => this.setState({email})}
                        autoCapitalize = 'none'
                    />
                </View>
                
                <View style={styles.inputContainer}>
                    {/* <Image style={styles.inputIcon} source={{uri: 'https://png.icons8.com/key-2/ultraviolet/50/3498db'}}/> */}
                    <TextInput style={styles.inputs}
                        placeholder="Password"
                        secureTextEntry={true}
                        underlineColorAndroid='transparent'
                        onChangeText={(password) => this.setState({password})}
                    />
                </View>

                <View style={styles.inputContainer}>
                    {/* <Image style={styles.inputIcon} source={{uri: 'https://png.icons8.com/key-2/ultraviolet/50/3498db'}}/> */}
                    <TextInput style={styles.inputs}
                        placeholder="Confirm Password"
                        secureTextEntry={true}
                        underlineColorAndroid='transparent'
                        onChangeText={(confirm_password) => this.setState({confirm_password})}
                    />
                </View>

                {/* <View style={{ flexDirection: 'column'}}>
                    
                    <View style={{ flexDirection: 'row' }}>
                        { checkbox }
                        <Text style={{marginTop: 5}}> I agree to the terms</Text>
                    </View>
                    </View> */}

                <TouchableHighlight 
                    style={[styles.buttonContainer, styles.loginButton]} 
                    onPress={this.onSubmit}
                    disabled={this.isLoading}
                >
                    { submitButton }
                </TouchableHighlight>

                <TouchableHighlight 
                    style={styles.textLink} 
                    onPress={() => this.props.navigation.replace('Login') }
                >
                    <Text>Log In</Text>
                </TouchableHighlight>

                <FlashMessage position="top" /> 
            </SafeAreaView>            
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#DCDCDC',
      },
      inputContainer: {
          borderBottomColor: '#F5FCFF',
          backgroundColor: '#eee',
          borderRadius:30,
          borderBottomWidth: 1,
          width:250,
          height:30,
          marginBottom:20,
          flexDirection: 'row',
          alignItems:'center'
      },
      inputs:{
          height:45,
          marginLeft:16,
          borderBottomColor: '#FFFFFF',
          flex:1,
      },
      inputIcon:{
        width:30,
        height:30,
        marginLeft:15,
        justifyContent: 'center'
      },
      buttonContainer: {
        height:45,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom:20,
        width:250,
        borderRadius:30,
      },
      loginButton: {
        backgroundColor: "#00b5ec",
      },
      loginText: {
        color: 'white',
      },
      textLink: {

      }
});

export default connect(
    mapStateToProps,
)(SignUpScreen);