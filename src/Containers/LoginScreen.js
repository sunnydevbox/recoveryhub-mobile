import React, { Component } from 'react';
import { connect } from 'react-redux';
import { 
    View, StyleSheet, TouchableOpacity, ActivityIndicator,
    Image, TextInput, TouchableHighlight, Text, Dimensions, SafeAreaView, Alert
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { UserService } from './../Services/UserService';
import { getToken, setToken } from './../Redux/AuthRedux';
import { Images, ApplicationStyles } from './../Themes';
// import { Field, reduxForm } from 'redux-form';
import FlashMessage, { showMessage, hideMessage } from 'react-native-flash-message';
import Config from './../../env';

console.log(Config);
function mapStateToProps(state) {
    return {
        auth: state.auth
    }
}

function mapDispatchToProps(dispatch) {
    return {
        setToken: token => { dispatch(setToken(token)) },
        getToken: () => { dispatch(getToken()) },
        
    };
}

class LoginScreen extends Component {
    static navigationOptions = {
        header: null,
    };

    constructor(props) {
        super(props);
        this.mounted = false;

        this.state = {
            username: null,
            password: null,
            isLoading: false,
            invalidCredentials: false,
        }

        this.onLogin = this.onLogin.bind(this);
        this.apiService = new UserService();
    }

    componentDidMount() {
        this.mounted = true;
     }

    onClickListener(target) {
        this.props.navigation.replace(target)
    }

    onLogin() {
        this.setState({isLoading: true});
        this.setState({invalidCredentials: false});

        this.apiService.login(this.state.username, this.state.password)
            .then((res) => {

                if (this.mounted) {
                    this.setState({isLoading: false});
                    this.props.setToken(res.data.token);
                    this.props.navigation.navigate('LoggedInStack');
                }
            })
            .catch((error) => {
                if (this.mounted) {
                    this.setState({isLoading: false});
                    showMessage({
                        message: 'Invalid email or password. Please try again.',
                        type: 'danger',
                        icon: { icon: "danger", position: "left" }
                    });
                }
            });
    }

    componentWillUnmount() {
        this.mounted = false;
        hideMessage();
    }

    render() {
        const { navigation } = this.props;

        const submitButton = (this.state.isLoading) ? <ActivityIndicator style={styles.loginText} /> : <Text style={styles.loginText}>Login</Text>;
        const invalidCredentials = (this.state.invalidCredentials) ? 'Invalid email or password' : '';
        let launchLogo = Images.launch;
        const dimensions = Dimensions.get('window');
        const imageHeight = Math.round(dimensions.width * 9 / 16);
        const imageWidth = dimensions.width - 100;

        if (navigation.getParam('registration', null)) {
            showMessage({
                message: 'Thank you for registering. Please check your email to verify your account.',
                type: 'success',
                icon: { icon: "success", position: "left" },
                // autoHide: false,
                // onPress: () => {
                //     hideMessage();
                // }
            });
        }
        

        return (
            <SafeAreaView style={ApplicationStyles.screen.centered}>
                <Image 
                  resizeMode={'contain'}
                  source={ Images.launch } style={{width: imageWidth }} 
                />
                <Text>[{Config.ENVIRONMENT}]</Text>
                <View style={styles.inputContainer}>
                    {/* <Image style={styles.inputIcon} source={{uri: 'https://png.icons8.com/message/ultraviolet/50/3498db'}}/> */}
                    <TextInput style={styles.inputs}
                        placeholder="Email"
                        keyboardType="email-address"
                        underlineColorAndroid='transparent'
                        onChangeText={(username) => this.setState({username})}
                        autoCapitalize = 'none'
                    />
                </View>
                
                <View style={styles.inputContainer}>
                    {/* <Image style={styles.inputIcon} source={{uri: 'https://png.icons8.com/key-2/ultraviolet/50/3498db'}}/> */}
                    <TextInput style={styles.inputs}
                        placeholder="Password"
                        secureTextEntry={true}
                        underlineColorAndroid='transparent'
                        onChangeText={(password) => this.setState({password})}
                    />
                </View>

                <TouchableHighlight 
                    style={[styles.buttonContainer, styles.loginButton]} 
                    onPress={this.onLogin}
                    disabled={this.isLoading}
                >
                    { submitButton }
                </TouchableHighlight>

                {/* <TouchableHighlight 
                    style={styles.textLink} 
                    onPress={() => this.onClickListener('ForgotPassword')}
                >
                    <Text>Forgot your password?</Text>
                </TouchableHighlight> */}

                <TouchableHighlight 
                    style={styles.textLink} 
                    onPress={() => this.onClickListener('SignUp')}
                    >
                    <Text>Register</Text>
                </TouchableHighlight>

                <FlashMessage position="top" /> 
                {/* <--- here as last component */}
            </SafeAreaView>            
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#DCDCDC',
      },
      inputContainer: {
          borderBottomColor: '#F5FCFF',
          backgroundColor: '#eee',
          borderRadius:30,
          borderBottomWidth: 1,
          width:250,
          height:45,
          marginBottom:20,
          flexDirection: 'row',
          alignItems:'center'
      },
      inputs:{
          height:45,
          marginLeft:16,
          borderBottomColor: '#FFFFFF',
          flex:1,
      },
      inputIcon:{
        width:30,
        height:30,
        marginLeft:15,
        justifyContent: 'center'
      },
      buttonContainer: {
        height:45,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom:20,
        width:250,
        borderRadius:30,
      },
      loginButton: {
        backgroundColor: "#00b5ec",
      },
      loginText: {
        color: 'white',
      },
      textLink: {

      }
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(LoginScreen);