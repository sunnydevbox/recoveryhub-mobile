import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Text } from 'react-native'

function mapStateToProps(state) {
    return {

    };
}

function mapDispatchToProps(dispatch) {
    return {

    };
}

class ForgotPasswordScreen extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View>
                <Text>Reset password</Text>
            </View>
        );
    }
}

export default connect(
    mapStateToProps,
)(ForgotPasswordScreen);