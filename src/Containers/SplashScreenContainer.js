import React, { Component } from 'react';
import { View, Text, Button, StyleSheet, Image, Dimensions, SafeAreaView } from 'react-native';
import { connect } from 'react-redux';
import { startup, setToken } from './../Redux/AuthRedux';
import AsyncStorage from '@react-native-community/async-storage';

import { Images, ApplicationStyles } from './../Themes';

function mapStateToProps(state) {
    return {
      auth: state.auth
    };
}

function mapDispatchToProps(dispatch) {
    return {
      startup: () => startup(),
      setToken: (token) => { dispatch(setToken(token)) }
    };
}

class SplashScreenContainer extends Component {
    
    static navigationOptions = {
        header: null
    };

    componentWillMount() {
      
        AsyncStorage.getItem('userToken')
        .then((token) => {
          if (token) {
            this.props.setToken(token);
            this.props.navigation.navigate('LoggedInStack');
          } else {
            this.props.navigation.navigate('NotLoggedInStack');
          }
        })
        .catch((data) => {
        })
      }

    render() {
      let launchLogo = Images.launch;
      const dimensions = Dimensions.get('window');
      const imageHeight = Math.round(dimensions.width * 9 / 16);
      const imageWidth = dimensions.width - 100;
      
        return (
            <SafeAreaView style={ApplicationStyles.screen.centered}>
                <Image 
                  resizeMode={'contain'}
                  source={ Images.launch } style={{width: imageWidth }} 
                />
                {/* <Button 
                    title="Login >>" 
                    onPress={() => { this.props.navigation.navigate('NotLoggedInStack')}}
                ></Button> */}
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#eee',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        
    }, 
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SplashScreenContainer);
