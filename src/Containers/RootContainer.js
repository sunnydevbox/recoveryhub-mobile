import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, StatusBar, StyleSheet } from 'react-native'
import Navigation from './../Navigation/Navigation';

function mapStateToProps(state) {
    return {};
}

function mapDispatchToProps(dispatch) {
    return {};
}

class RootContainer extends Component {
    
    componentDidMounat() {
        // CHECK SESSION HERE
    }
    
    render() {
        return (   
            <View style={styles.container}>
                <StatusBar barStyle='dark-content' />
                <Navigation />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
    //   justifyContent: 'center',
    //   alignItems: 'center',
      backgroundColor: '#F5FCFF',
    }
  });
  

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(RootContainer);

