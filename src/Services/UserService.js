import { create } from 'apisauce';
let { API } = require('./AbstractAPI');


export class UserService extends API {

    endpoint = 'users';

    login(username, password) {
        console.log(this.apiURL() + '/auth');
        return this.instance.post(this.apiURL() + '/auth', {
            email: username,
            password:password,
        })
        .then((res) => {
            if (!res.ok) {
                return this.handleError(res);
            }
            return res.data;
        })
        ;
    }

    register(data = {}) {
        return this.instance.post(this.apiURL() + '/register', JSON.stringify(data))
        .then((res) => {
            if (!res.ok) {
                return this.handleError(res);
            }

            return res.data;
        })
        
    }
}