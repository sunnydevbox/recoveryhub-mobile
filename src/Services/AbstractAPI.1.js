
// import fetch from 'fetch';
import 'whatwg-fetch'
import React, { Component } from 'react';

const API_URL = 'http://api.recoveryhub.world/api/';
const endpoint = 'users';

export class API {
  
  headers() {
    return {
      // Accept: HEADER_ACCEPT,
      // 'Content-Type': CONTENT_TYPE,
    }
  }


  endpoint(endpoint) {
    
    if (!endpoint) {
      endpoint = 'users'
    }
    return this.baseUrl() + endpoint;
  }

  baseUrl() {
    return API_URL;
  }
  
  index(queryString = '') {

    let headers = this.headers();

    return fetch(this.endpoint(), {
      method: 'POST',
      headers,
      // body: JSON.stringify(payload)
    }).then((res) => {
      return res.json().then(json => ({ json, res }));
    }).then(({json, res}) => {
      return json;
      // if (!res.ok) {
      //   return Promise.reject(json);
      // }

      // return json;
    });
  }

  show(id, queryString = '') {

  }

  post() {

  }

  delete() {

  }

  update() {
    
  }

}


// export const API = {
//   path: () => {

//   },


//   post: (path, payload, token) => {
//     let headers = {
//       'Accept': 'application/json',
//       'Content-Type': 'application/json'
//     };

//     if (token) {
//       headers['Authorization'] = 'Bearer ' + token;
//     };

//     return fetch(API_URL + path, {
//       method: 'POST',
//       headers,
//       body: JSON.stringify(payload)
//     }).then((res) => {
//       return res.json().then(json => ({ json, res }));
//     }).then(({json, res}) => {
//       if (!res.ok) {
//         return Promise.reject(json);
//       }

//       return json;
//     });
//   }
// };