
import React, { Component } from 'react';
import { create } from 'apisauce';
import NavigationService from './NavigationService';
import Config from './../../env';

console.log(Config);
const API_URL = Config.API_URL;
const API_HEADER_ACCEPT = Config.API_HEADER_ACCEPT;

// define the api
const axiosAPI = create({
    baseURL: API_URL,
    headers: { 
        Accept: API_HEADER_ACCEPT,
        'Content-type': 'application/json'
    },
});


/************************/
/**** INTERCEPTORS *****/
/************************/
axiosAPI.axiosInstance.interceptors.request.use(
  function (config) {
    // Do something before request is sent

    console.log('int', config);
    return config;
  }, 
  function (error) {
    // Do something with request error
    return Promise.reject(error);
  }
);


axiosAPI.axiosInstance.interceptors.response.use(
  function (response) {
    
    // Do something with response data
    return response;
  }, 
  function (error) {
    // console.log('Navvv', NavigationService.getCurrentRoute());
    // Do something with response error
    if (error.response.status == 401) {
      // console.log('Redirect to login')

      // NavigationService.navigate('LoggedInStack', { userName: 'Lucy' });
    }

    return Promise.reject(error);
  }
);


export class API {
  endpoint = null;
  instance = axiosAPI;

  apiURL(endpoint?) {
    let e = (endpoint) ? endpoint : this.endpoint;
    return API_URL + e; 
  }

  index(queryString = '') {
    return axiosAPI.get(this.apiURL());
  }

  show(id, params?) {
    return axiosAPI.get(this.apiURL() + id + '?' + this.buildQueryString(params))
    .then((res) => {
      if (!res.ok) {
          return this.handleError(res);
      }

      return res.data;
    });
  }

  update(id, data = {}) {
    return axiosAPI.patch(this.apiURL() + id, data)
    .then((res) => {
      if (!res.ok) {
          return this.handleError(res);
      }

      return res.data;
    });
  }

  destroy(id, data = {}) {
    return axiosAPI.delete(this.apiURL() + id)
    .then((res) => {
      if (!res.ok) {
          return this.handleError(res);
      }

      return res.data;
    });
  }


  post(data = {}) {
    return axiosAPI.post(this.apiURL(), data)
    .then((res) => {
        if (!res.ok) {
            return this.handleError(res);
        }

        return res.data;
    });
  }

  handleError(error) {
    return Promise.reject(error);
  }

  buildQueryString(filterQuery?) {
    let sq = '';
    for (const filter in filterQuery) {
        if (typeof(filter) === 'string') {
            sq += filter + '=' + filterQuery[filter] + '&';
        } else if (typeof(filter) === 'object') {
            Object.keys(filter).map(
                (key) => {
                    sq += key + '=' + filter[key];
                }
            );
        } else {}
    }
    return sq;
  }
}