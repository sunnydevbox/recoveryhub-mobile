/** REDUCERS  */
export const reducers = (state = { username: null, token: null }, action) => {
  switch (action.type) {
    case "SET_USER":
      const newState = state;
      newState.username = action.payload.username;
      newState.token = action.payload.token;
      return newState;
      break;
    default:
      return state;
      break;
  }

  return state;
};

/* ACTIONS */

export const actions = {};
