import ReactNative from "react-native";
import AsyncStorage from '@react-native-community/async-storage';


/** REDUCERS  */
export const reducers = (state = { data: {}, token: null }, action) => {
  const newState = state;
  
  switch (action.type) {
    case 'SET_USER':    
      // PERSIST token
      return { ...state, user: action.user };
      break;
    
    case 'SET_TOKEN':
      // PERSIST token
      // token = 

      AsyncStorage.setItem('userToken', action.token);
      return { ...state, token: action.token };
      break;

    // case 'GET_TOKEN':
    //   // AsyncStorage.getItem('userToken');
      
    //   break;

    case 'CLEAR_AUTH':
        console.log('LOGGING OUT')
        newState.data = {};
        newState.token = null;
        return {...state, newState};
      break;

    default:
      return state;
      break;
  }

  return state;
};

/* ACTIONS */
export const getToken = () => ({
  type: 'GET_TOKEN'
});

export const setToken = (token) => ({
  type: 'SET_TOKEN',
  token
});

export const removeToken = () => ({
  type: 'REMOVE_TOKEN',
});

export const logout = () => ({
  type: 'CLEAR_AUTH',
});

export const loading = bool => ({
  type: 'LOADING',
  isLoading: bool,
});

export const error = error => ({
  type: 'ERROR',
  error,
});

export const getUserToken = () => dispatch => {
  console.log(3);
 return AsyncStorage.getItem('userToken')
        .then((data) => {
            dispatch(loading(false));
            dispatch(getToken(data));
        })
        .catch((err) => {
            dispatch(loading(false));
            dispatch(error(err.message || 'ERROR'));
        })

      }

export const saveUserToken = (data) => dispatch => {
  console.log(2);
    AsyncStorage.setItem('userToken', 'abc')
        .then((data) => {
            dispatch(loading(false));
            dispatch(saveToken('token saved'));
        })
        .catch((err) => {
            dispatch(loading(false));
            dispatch(error(err.message || 'ERROR'));
        })

      }

export const removeUserToken = () => dispatch =>
    AsyncStorage.removeItem('userToken')
        .then((data) => {
            dispatch(loading(false));
            dispatch(removeToken(data));
        })
        .catch((err) => {
            dispatch(loading(false));
            dispatch(error(err.message || 'ERROR'));
        })

export const startup = () => dispatch =>
  AsyncStorage.getItem('userToken')
      .then((token) => {
        // console.log(token)
        dispatch(setToken(token));

        return token
      }) 