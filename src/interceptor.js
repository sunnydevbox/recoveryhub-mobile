import fetchIntercept from 'fetch-intercept';
 
// PUT IN A CONFIG FILE
const API_URL = 'http://cebuunitedrebuilders-api.localhost/api/';
const HEADER_ACCEPT = 'application/x.metropsych.v1+json';
const CONTENT_TYPE = 'application/json';



export const unregister = fetchIntercept.register({
    request: function (url, config) {
        // if (!config['headers']) {
        //     console.log(config)
        //     // config['headers'] = {};
        // }
        
        config.headers['Accept'] = HEADER_ACCEPT;
        config.headers['Content-Type'] = CONTENT_TYPE;
        
        // AUTHORIZATION
        // IF token exists
        //config.headers['Authorization'] = 'Bearer ' + 'asd';
        
        
        // Modify the url or config here
        return [url, config];
    },
 
    requestError: function (error) {

        console.log('requestError' , error)
        // Called when an error occured during another 'request' interceptor call
        return Promise.reject(error);
    },
 
    response: function (response) {
        console.log('response' , response)

        if (response.status == 401) {

            // REDIRECT to login page if not yet there
            console.log('redirect o login error: ', response)
        }
        // Modify the reponse object
        return response;
    },
 
    responseError: function (error) {
        console.log('responseError' , error)
        // Handle an fetch error
        return Promise.reject(error);
    }
});
 
// // Call fetch to see your interceptors in action.
// fetch('http://google.com');
 
// // Unregister your interceptor
// unregister();